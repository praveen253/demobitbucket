package com.javainuse.config;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = -7858869558953243875L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		 Map<String, Object> mapError = new HashMap<String, Object>();
		 mapError.put("timestamp", new Date());
		 mapError.put("status", 403);
		 mapError.put("message", "Access denied");
		
		 response.setContentType("application/json;charset=UTF-8");
		 response.setStatus(403);
		 response.getWriter().write(mapError.toString());

//		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Praveen Kumar Samal");

	}
}