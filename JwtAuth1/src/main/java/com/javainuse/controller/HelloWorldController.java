package com.javainuse.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/usermsg")
	public String firstPageu() {
		return "User Hello World !!!";
	}

	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	@RequestMapping(value = "/403")
	public String first403() {
		return "403 Error";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/adminmsg")
	public String firstPagea() {
		return "Admin Hello World !!!!!!!";
	}

	@RequestMapping(value = "/ulogout")
	public String logoutPage() {
		return "You are success fully logged out!!!";
	}
}