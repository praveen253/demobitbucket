package com.javainuse.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.exentity.ExpEntity;

@Repository
public interface ExEntityRepo extends CrudRepository<ExpEntity, Integer> {
	Optional<ExpEntity> findByToken(String token);
}
