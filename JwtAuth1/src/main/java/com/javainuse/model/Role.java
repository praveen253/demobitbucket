package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "Role")
@SequenceGenerator(name = "role_seq", initialValue = 1)
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_seq")
	@Column(name = "role_id")
	private int roleId;

	@Column(name = "role")
	private String role;

	public Role() {
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
