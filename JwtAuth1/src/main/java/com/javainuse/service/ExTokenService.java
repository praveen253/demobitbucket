package com.javainuse.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javainuse.dao.ExEntityRepo;
import com.javainuse.exentity.ExpEntity;

@Service
public class ExTokenService {
	@Autowired
	private ExEntityRepo extokenrepo;

	public Optional<ExpEntity> findByExToken(String token) {
		Iterable<ExpEntity> listData = extokenrepo.findAll();
//		Optional<ExpEntity> uentity;
//		if (listData.size() != 0)
//			uentity = Optional.of(listData.get(0));
//		else
//			uentity = Optional.of(null);
		return extokenrepo.findByToken(token);
	}

	public ExpEntity save(String token) {
		ExpEntity exdata = new ExpEntity();
		exdata.setToken(token);
		return extokenrepo.save(exdata);
	}
}
