package com.kafswa.fl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaSwaggerflApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaSwaggerflApplication.class, args);
	}
}
