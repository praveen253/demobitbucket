package com.kafswa.fl.restcontroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping("/product")
public class SwaggerAPIController {

	private static Logger LOGGER = LogManager.getLogger(SwaggerAPIController.class);

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<String> getProducts() {
		List<String> productsList = new ArrayList<>();
		productsList.add("Honey");
		productsList.add("Almond");
		LOGGER.info("List of all the data");
		return productsList;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String createProduct() {
		LOGGER.info("Product is saved successfully");
		return "Product is saved successfully";
	}
}