package com.javasampleapproach.redis.repo;

import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.redis.model.Student;
 
public interface StudentRepository extends CrudRepository<Student, String> {
 
}